//s30 activity

//Number 1 - answer
db.fruits.aggregate([
    {$match: {$and: [{supplier: "Yellow Farms"}, {price : {$lt:50}}]}},
    {$count: "yellowFarmsLowerThan50"}
  ])

//Number 2 - answer
db.fruits.aggregate([
    {$match : {price: {$lt: 30}}},
    {$count: "priceLesserThan30"}
 ])

 //Number 3 - answer
 db.fruits.aggregate([
    {$match: {supplier: "Yellow Farms"}},
    {$group : {_id: "$supplier", avgPrice: {$avg: "$price"}}}

])

//number 4 answer
db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."}},
    {$group : {_id: "$supplier", maxPrice: {$max: "$price"}}}

])

//number 5 answer
db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."}},
    {$group : {_id: "$supplier", minPrice: {$min: "$price"}}}

])
